import * as React from 'react';
import './NavBar.css';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import DvrIcon from '@mui/icons-material/Dvr';
import LogoutIcon from '@mui/icons-material/Logout';
import PeopleIcon from '@mui/icons-material/People';
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser';
import Stack from '@mui/material/Stack';

import "@fontsource/roboto";
import "@fontsource/open-sans";
import "@fontsource/montserrat";


// const pages = ['Login', 'Register'];
// const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

const NavBar = () => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar className="app-bar-1" position="static" sx={{ bgcolor: 'success.main', justifyContent: 'center' }}>
      <Container maxWidth="xl">
        <Toolbar disableGutters height="max-content" className="toolbar-1">

          {/* ////////////////////////////////////////////// */}
          {/* DESKTOP VERZIJA - NASLOVA */}

          <Typography
            variant="h5"
            noWrap
            component="a"
            href="/home-admin"
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              // fontFamily: 'Montserrat',
              // fontWeight: 900,
              letterSpacing: '.1rem',
              fontSize: '1.75rem',
              textTransform: 'uppercase',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            Karbonski otisak
          </Typography>

          {/* ////////////////////////////////////////////// */}
          {/* MOBILNA VERZIJA  - MENIJA */}

          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              sx={{ ml: -1 }}
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              href="/home-admin"
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              <MenuItem onClick={handleCloseNavMenu}>
                <Typography
                  textAlign="center"
                  justifyContent="flex-end"
                  noWrap
                  component="a"
                  href='/home-admin'
                  className="pages-mobile-menu"
                  sx={{
                    fontSize: '1rem',
                    fontFamily: 'Open Sans',
                    fontWeight: 900,
                    letterSpacing: '.1rem',
                    // textTransform: 'uppercase',
                    color: 'inherit',
                    textDecoration: 'none'
                  }}
                >
                  Korisnici
                </Typography>
              </MenuItem>

              <MenuItem onClick={handleCloseNavMenu}>
                <Typography
                  textAlign="center"
                  justifyContent="flex-end"
                  noWrap
                  component="a"
                  href='/verified-list'
                  className="pages-mobile-menu"
                  sx={{
                    fontSize: '1rem',
                    fontFamily: 'Open Sans',
                    fontWeight: 900,
                    letterSpacing: '.1rem',
                    // textTransform: 'uppercase',
                    color: 'inherit',
                    textDecoration: 'none'
                  }}
                >
                  Verifikuj
                </Typography>
              </MenuItem>

              <MenuItem onClick={handleCloseNavMenu}>
                <Typography
                  textAlign="center"
                  justifyContent="flex-end"
                  noWrap
                  component="a"
                  href='/ranglist'
                  className="pages-mobile-menu"
                  sx={{
                    fontSize: '1rem',
                    fontFamily: 'Open Sans',
                    fontWeight: 900,
                    letterSpacing: '.1rem',
                    // textTransform: 'uppercase',
                    color: 'inherit',
                    textDecoration: 'none'
                  }}
                >
                  Rang lista
                </Typography>
              </MenuItem>

              <MenuItem onClick={handleCloseNavMenu}>
                <Typography
                  textAlign="center"
                  justifyContent="flex-end"
                  noWrap
                  component="a"
                  href='/'
                  className="pages-mobile-menu"
                  sx={{
                    fontSize: '1rem',
                    fontFamily: 'Open Sans',
                    fontWeight: 900,
                    letterSpacing: '.1rem',
                    // textTransform: 'uppercase',
                    color: 'inherit',
                    textDecoration: 'none'
                  }}
                >
                  Odjavi se
                </Typography>
              </MenuItem>
              
            </Menu>
          </Box>

          {/* ////////////////////////////////////////////// */}
          {/* MOBILNA VERZIJA  - NASLOVA */}

          <Typography
            variant="h6"
            noWrap
            component="a"
            // href="/"
            sx={{
              mr: 1,
              my: 2,
              display: { xs: 'flex', md: 'none' },
              flexGrow: 1,
              // fontFamily: 'Montserrat',
              // fontWeight: 900,
              // fontSize: '1.8rem',
              textTransform: 'uppercase',
              letterSpacing: '.15rem',
              color: 'inherit',
              textDecoration: 'none'
            }}
          >
            Karbonski otisak
          </Typography>
          
          {/* ////////////////////////////////////////////// */}
          {/* DESKTOP VERZIJA - MENIJA */}

          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex', justifyContent: 'flex-end' } }}>
            <Stack direction="row" spacing={1}>
            <Button
              direction="row"
              endIcon={<PeopleIcon />}
              onClick={handleCloseNavMenu}
              href="/home-admin"
              className="pages-desktop-menu"
              sx={{
                my: 2,
                color: 'white',
                textTransform: 'none',
                // fontFamily: 'Open Sans',
                // fontWeight: 900,
                fontSize: '1rem',
                letterSpacing: '.1rem',
                fontWeight: '400',
                textDecoration: 'none'
              }}
            >
              Korisnici
            </Button>
            <Button
                direction="row"
                endIcon={<VerifiedUserIcon />}
                onClick={handleCloseNavMenu}
                href="/verified-list"
                className="pages-desktop-menu"
                sx={{
                  my: 2,
                  color: 'white',
                  textTransform: 'none',
                  // fontFamily: 'Open Sans',
                  // fontWeight: 900,
                  fontSize: '1rem',
                  letterSpacing: '.1rem',
                  fontWeight: '400',
                  textDecoration: 'none'
                }}
              >
                Verifikuj
              </Button>

              <Button
                direction="row"
                endIcon={<DvrIcon />}
                key="Login"
                onClick={handleCloseNavMenu}
                href="/ranglist"
                className="pages-desktop-menu"
                sx={{
                  my: 2,
                  color: 'white',
                  textTransform: 'none',
                  // fontFamily: 'Open Sans',
                  // fontWeight: 900,
                  fontSize: '1rem',
                  letterSpacing: '.1rem',
                  fontWeight: '400',
                  textDecoration: 'none'
                }}
              >
                Rang lista
              </Button>

              <Button
                direction="row"
                endIcon={<LogoutIcon />}
                key="Register"
                onClick={handleCloseNavMenu}
                href="/"
                className="pages-desktop-menu"
                sx={{
                  my: 2,
                  color: 'white',
                  textTransform: 'none',
                  // fontFamily: 'Open Sans',
                  // fontWeight: 900,
                  fontSize: '1rem',
                  letterSpacing: '.1rem',
                  fontWeight: '400',
                  textDecoration: 'none'
                }}
              >
                Odjavi se
              </Button>
            </Stack>
          </Box>

          {/* <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex', justifyContent: 'flex-end' } }}>
            {pages.map((page) => (
              <Button
                key={page}
                onClick={handleCloseNavMenu}
                href={page}
                className="pages-desktop-menu"
                sx={{
                  mr: 2,
                  my: 2,
                  color: 'white',
                  display: 'block',
                  fontFamily: 'Open Sans',
                  fontWeight: 900,
                  fontSize: '1.25rem',
                  letterSpacing: '.1rem',
                  fontWeight: '400',
                  textDecoration: 'none'
                }}
              >
                {page}
              </Button>
            ))}
          </Box> */}

        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default NavBar;
