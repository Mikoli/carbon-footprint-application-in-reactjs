import React from 'react';
import { useNavigate } from 'react-router-dom';
import './NavBar.css';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import TaxiAlertIcon from '@mui/icons-material/TaxiAlert';
import LogoutIcon from '@mui/icons-material/Logout';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import Stack from '@mui/material/Stack';
import Avatar from '@mui/material/Avatar';
import Link from '@mui/material/Link';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';
import HomeIcon from '@mui/icons-material/Home';

import "@fontsource/roboto";
import "@fontsource/open-sans";
import "@fontsource/montserrat";



function stringAvatar(name) {
    let ulogovaniKorisnik = JSON.parse(localStorage.getItem("loginUser"));
    return {
        children: `${ulogovaniKorisnik.username.split(' ')[0][0]}`
    };
}


const NavBarActivities = () => {
    const navigate = useNavigate()

    const logOut = () => {
        navigate('/');
        navigate(0);
        localStorage.removeItem('loginUser')
    }
    const [anchorElNav, setAnchorElNav] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    return (
        <AppBar className="app-bar-1" position="static" sx={{ bgcolor: 'success.main', justifyContent: 'center' }}>
            <Container maxWidth="xl">
                <Toolbar disableGutters height="max-content" className="toolbar-1">

                    {/* ////////////////////////////////////////////// */}
                    {/* DESKTOP VERZIJA - NASLOVA */}

                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href="/home"
                        sx={{
                            mr: 2,
                            display: { xs: 'none', md: 'flex' },
                            // fontFamily: 'Montserrat',
                            // fontWeight: 900,
                            letterSpacing: '.1rem',
                            fontSize: '1.75rem',
                            textTransform: 'uppercase',
                            color: 'inherit',
                            textDecoration: 'none',
                        }}
                    >
                        Karbonski otisak
                    </Typography>

                    {/* ////////////////////////////////////////////// */}
                    {/* MOBILNA VERZIJA  - MENIJA */}

                    <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                        <IconButton
                            sx={{ ml: -1 }}
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            // href="/"
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: 'block', md: 'none' },
                            }}
                        >
                            <MenuItem onClick={handleCloseNavMenu}>
                                <Typography
                                    textAlign="center"
                                    justifyContent="flex-end"
                                    noWrap
                                    component="a"
                                    href='/home'
                                    className="pages-mobile-menu"
                                    sx={{
                                        fontSize: '1rem',
                                        fontFamily: 'Open Sans',
                                        fontWeight: 900,
                                        letterSpacing: '.1rem',
                                        // textTransform: 'uppercase',
                                        color: 'inherit',
                                        textDecoration: 'none'
                                    }}
                                >
                                    Početna
                                </Typography>
                            </MenuItem>

                            <MenuItem onClick={handleCloseNavMenu}>
                                <Typography
                                    textAlign="center"
                                    justifyContent="flex-end"
                                    noWrap
                                    component="a"
                                    href='/activities'
                                    className="pages-mobile-menu"
                                    sx={{
                                        fontSize: '1rem',
                                        fontFamily: 'Open Sans',
                                        fontWeight: 900,
                                        letterSpacing: '.1rem',
                                        // textTransform: 'uppercase',
                                        color: 'inherit',
                                        textDecoration: 'none'
                                    }}
                                >
                                    Aktivnosti
                                </Typography>
                            </MenuItem>

                            <MenuItem onClick={handleCloseNavMenu}>
                                <Typography
                                    textAlign="center"
                                    justifyContent="flex-end"
                                    noWrap
                                    component="a"
                                    href='/'
                                    className="pages-mobile-menu"
                                    sx={{
                                        fontSize: '1rem',
                                        fontFamily: 'Open Sans',
                                        fontWeight: 900,
                                        letterSpacing: '.1rem',
                                        // textTransform: 'uppercase',
                                        color: 'inherit',
                                        textDecoration: 'none'
                                    }}
                                >
                                    Odjavi se
                                </Typography>
                            </MenuItem>
                        </Menu>
                    </Box>

                    {/* ////////////////////////////////////////////// */}
                    {/* MOBILNA VERZIJA  - NASLOVA */}

                    <Typography
                        variant="h6"
                        noWrap
                        component="a"
                        href="/home"
                        sx={{
                            mr: 1,
                            my: 2,
                            display: { xs: 'flex', md: 'none' },
                            flexGrow: 1,
                            // fontFamily: 'Montserrat',
                            // fontWeight: 900,
                            // fontSize: '1.8rem',
                            textTransform: 'uppercase',
                            letterSpacing: '.15rem',
                            color: 'inherit',
                            textDecoration: 'none'
                        }}
                    >
                        Karbonski otisak
                    </Typography>

                    {/* ////////////////////////////////////////////// */}
                    {/* DESKTOP VERZIJA - MENIJA */}

                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex', justifyContent: 'flex-end' } }}>
                        <Stack direction="row" spacing={1}>
                            <Button
                                direction="row"
                                endIcon={<HomeIcon />}
                                href="/home"
                                className="pages-desktop-menu"
                                sx={{
                                    my: 2,
                                    color: 'white',
                                    textTransform: 'none',
                                    // fontFamily: 'Open Sans',
                                    // fontWeight: 900,
                                    fontSize: '1rem',
                                    letterSpacing: '.1rem',
                                    fontWeight: '400',
                                    textDecoration: 'none'
                                }}
                            >
                                Početna
                            </Button>

                            <Button
                                direction="row"
                                endIcon={<TaxiAlertIcon />}
                                href="/activities"
                                className="pages-desktop-menu"
                                sx={{
                                    my: 2,
                                    color: 'white',
                                    textTransform: 'none',
                                    // fontFamily: 'Open Sans',
                                    // fontWeight: 900,
                                    fontSize: '1rem',
                                    letterSpacing: '.1rem',
                                    fontWeight: '400',
                                    textDecoration: 'none'
                                }}
                            >
                                Aktivnosti
                            </Button>

                            <Button
                                direction="row"
                                endIcon={<LogoutIcon />}
                                onClick={logOut}
                                href="/"
                                className="pages-desktop-menu"
                                sx={{
                                    my: 2,
                                    pr: 2,
                                    color: 'white',
                                    textTransform: 'none',
                                    // fontFamily: 'Open Sans',
                                    // fontWeight: 900,
                                    fontSize: '1rem',
                                    letterSpacing: '.1rem',
                                    fontWeight: '400',
                                    textDecoration: 'none'
                                }}
                            >
                                Izloguj se
                            </Button>

                        </Stack>
                    </Box>

                    <Link href='/profile' style={{ textDecoration: 'none' }}>
                        <Tooltip TransitionComponent={Zoom} title="Profil" arrow >
                            <Avatar
                                variant="circular"
                                sx={{ bgcolor: '#9ffb54'/* , ml: 2 */ }}
                                {...stringAvatar()}
                            />
                        </Tooltip>
                    </Link>

                </Toolbar>
            </Container>
        </AppBar>
    );
};
export default NavBarActivities;
