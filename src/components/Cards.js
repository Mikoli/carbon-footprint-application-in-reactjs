import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea, Grid } from '@mui/material';



const Cards = () => {
    return (
        <Grid container spacing={2}>
            <Grid item xs={12} sm={4}>
                <Card >
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140" 
                            image={require('../pages/Picture/Diesel1.jpg')}
                            alt="Automobil na dizel"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Automobil na dizel
                            </Typography>
                            <Typography sx={{ color: 'blue' }} variant="body2" color="text.secondary">
                                Karbonski otisak je 27.96 kg.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Grid>
                <Grid item xs={12} sm={4}>
                <Card >
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140" 
                            image={require('../pages/Picture/Benzine1.jpg')}
                            alt="Automobil na benzin"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Automobil na benzin
                            </Typography>
                            <Typography sx={{ color: 'blue' }} variant="body2" color="text.secondary">
                                Karbonski otisak je 23.74 kg.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Grid>
                <Grid item xs={12} sm={4}>
                <Card >
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140" 
                            image={require('../pages/Picture/Taxi1.jpg')}
                            alt="Taxi"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Taxi
                            </Typography>
                            <Typography sx={{ color: 'blue' }} variant="body2" color="text.secondary">
                                Karbonski otisak je 14.29 kg.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Grid>
                <Grid item xs={12} sm={4}>
                <Card >
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140"
                            image={require('../pages/Picture/Economic2.jpg')}
                            alt="Let u ekonomskoj klasi"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Let u ekonomskoj klasi
                            </Typography>
                            <Typography sx={{ color: 'blue' }} variant="body2" color="text.secondary">
                                Karbonski otisak je 8.70 kg.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Grid>
                <Grid item xs={12} sm={4}>
                <Card >
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140"
                            image={require('../pages/Picture/Business-class-2.jpg')}
                            alt="Let u biznis klasi"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Let u biznis klasi
                            </Typography>
                            <Typography sx={{ color: 'blue' }} variant="body2" color="text.secondary">
                                Karbonski otisak je 18.77 kg.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Grid>
                <Grid item xs={12} sm={4}>
                <Card >
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140"
                            image={require('../pages/Picture/Firstclass1.jpg')}
                            alt="Let u prvoj klasi"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Let u prvoj klasi
                            </Typography>
                            <Typography sx={{ color: 'blue' }} variant="body2" color="text.secondary">
                                Karbonski otisak je 23.36 kg.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Grid>
                <Grid item xs={12} sm={4}>
                <Card >
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140"
                            image={require('../pages/Picture/Motorbike2.jpg')}
                            alt="Motocikl"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Motocikl
                            </Typography>
                            <Typography sx={{ color: 'blue' }} variant="body2" color="text.secondary">
                                Karbonski otisak je 12.43 kg.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Grid>
                <Grid item xs={12} sm={4}>
                <Card >
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140"
                            image={require('../pages/Picture/Bus1.png')}
                            alt="Autobus"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Autobus
                            </Typography>
                            <Typography sx={{ color: 'blue' }} variant="body2" color="text.secondary">
                                Karbonski otisak je 6.65 kg.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Grid>
                <Grid item xs={12} sm={4}>
                <Card >
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            height="140"
                            image={require('../pages/Picture/Rail2.jpg')}
                            alt="Tranzitna železnica"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Tranzitna železnica
                            </Typography>
                            <Typography sx={{ color: 'blue' }} variant="body2" color="text.secondary">
                                Karbonski otisak je 10.13 kg.
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Grid>
            </Grid>
    )
}

export default Cards