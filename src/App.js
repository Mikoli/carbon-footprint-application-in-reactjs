import React from "react";
import { BrowserRouter as HashRouter, Route, Routes } from 'react-router-dom'
import HomePageAuth from './pages/HomePageAuth';
import Login from './pages/Auth/Login';
import Register from './pages/Auth/Register';
import HomePage from './pages/HomePage';
import ProfilePage from "./pages/ProfilePage";
import Activities from "./pages/Activities";
import ChangePassword from "./pages/Forms/ChangePassword";
import HomePageAdmin from "./pages/Private/HomePageAdmin";
import RangList from "./pages/Private/RangList";
import PrivateRoutes from "./utilis/PrivateRoutes";
import VerifiedList from "./pages/Private/VerifiedList";


function App() {
  return (
    <HashRouter>
      <Routes>
        <Route element={<PrivateRoutes />}>
        <Route path='/verified-list' element={<VerifiedList />} />
        <Route path='/home-admin' element={<HomePageAdmin />} />
        <Route path='/ranglist' element={<RangList />} />
        </Route>
        <Route path='/' element={<HomePageAuth />} />
        <Route path='/login' element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path='/home' element={<HomePage />} />
        <Route path='/profile' element={<ProfilePage />} />
        <Route path='/activities' element={<Activities />} />
        <Route path='/profile-change-password' element={<ChangePassword />} />
      </Routes>
    </HashRouter>
  );
}
export default App;
