import React, { useState, useEffect } from 'react';
import Img from './Picture/Img.jpg';
import NavBarActivities from '../components/NavBarActivities';
import './Activities.css';

import { Button, Paper } from '@mui/material';
import axios from 'axios';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from "@mui/material/Typography";
import Icon from '@mui/material/Icon';
import DepartureBoardIcon from '@mui/icons-material/DepartureBoard';
import ShutterSpeedIcon from '@mui/icons-material/ShutterSpeed';
import FlagIcon from '@mui/icons-material/Flag';
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import LocalGasStationIcon from '@mui/icons-material/LocalGasStation';
import Link from '@mui/material/Link';


function Copyright(props) {
  return (
    <Typography className="copyright" variant="body2" color="#fff" align="center" {...props}>
      {'Copyright©'}
      <Link color="inherit" href="https://mui.com/">
        www.karbonskiotisak.com
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}

const Activities = () => {

  const [podaciZaSlanje, setPodaciZaSlanje] = useState({})

  const restart = () => {
    document.location.reload(true)
  }

  const request1 = () => {
    console.log(podaciZaSlanje.brojMiljaIliGoriva);
    console.log(podaciZaSlanje.razdaljinaIliGorivo);
    console.log(podaciZaSlanje.zemlja);
    console.log(podaciZaSlanje.vrstaPrevoza);
    // Prethodne 4 vrednosti iz konzole se prosledjuju GET metodom u sklopu URL-a.
    console.log(podaciZaSlanje);
    if (podaciZaSlanje.razdaljinaIliGorivo === 'miles') {
      if (podaciZaSlanje.brojMiljaIliGoriva === undefined ||
        podaciZaSlanje.razdaljinaIliGorivo === undefined ||
        podaciZaSlanje.zemlja === undefined ||
        podaciZaSlanje.vrstaPrevoza === undefined) {
        alert('Sva polja moraju biti popunjena osim za vrstu goriva');
        return;
      }
      axios.get(`https://api.triptocarbon.xyz/v1/footprint?activity=${podaciZaSlanje.brojMiljaIliGoriva}&activityType=${podaciZaSlanje.razdaljinaIliGorivo}&country=${podaciZaSlanje.zemlja}&mode=${podaciZaSlanje.vrstaPrevoza}`)
        // Resen problem sa linkom, ne sme da se prelama u novi red pa se to devinise kao razmaci zbog kojih se pojavljuju '20%'
        .then(function (response) {
          // handle success
          let loginUser = JSON.parse(localStorage.getItem("loginUser"));
          console.log(response);
          console.log(response.data);
          console.log(response.data.carbonFootprint);
          console.log(`Karbonski otisak je ${response.data.carbonFootprint}`)
          let typography = document.getElementById('target')
          typography.innerHTML = `Karbonski otisak je ${response.data.carbonFootprint}`
          let localUsers = JSON.parse(localStorage.getItem("users"));
          // let loginUser = JSON.parse(localStorage.getItem("loginUser"));
          localUsers.forEach(user => { // Menjanje registrovanog korisnika.
            if (user.username === loginUser.username) {
              if (!user.hasOwnProperty('carbonFootprint')) {
                user.carbonFootprint = Number(response.data.carbonFootprint);
              } else {
                user.carbonFootprint += Number(response.data.carbonFootprint);
              }
            }
          })
          if (!loginUser.hasOwnProperty('carbonFootprint')) {
            loginUser.carbonFootprint = Number(response.data.carbonFootprint);
          } else {
            loginUser.carbonFootprint += Number(response.data.carbonFootprint);
          }
          localStorage.setItem("loginUser", JSON.stringify(loginUser));
          localStorage.setItem("users", JSON.stringify(localUsers));
          let typography2 = document.getElementById('target2');
          typography2.innerHTML = `Vaš ukupan karbonski otisak je ${loginUser.carbonFootprint.toFixed(2)}`;
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed
        });

    } else { // podaciZaSlanje.razdaljinaIliGorivo === 'fuel'
      if (podaciZaSlanje.brojMiljaIliGoriva === undefined ||
        podaciZaSlanje.razdaljinaIliGorivo === undefined ||
        podaciZaSlanje.zemlja === undefined ||
        podaciZaSlanje.vrstaGoriva === undefined
      ) {
        alert('Sva polja moraju biti popunjena osim za vrstu prevoza');
        return;
      }
      axios.get(`https://api.triptocarbon.xyz/v1/footprint?activity=${podaciZaSlanje.brojMiljaIliGoriva}&activityType=${podaciZaSlanje.razdaljinaIliGorivo}&country=${podaciZaSlanje.zemlja}&fuelType=${podaciZaSlanje.vrstaGoriva}`)
        .then(function (response) {
          // handle success
          let loginUser = JSON.parse(localStorage.getItem("loginUser"));
          console.log(response);
          console.log(response.data);
          console.log(response.data.carbonFootprint);
          console.log(`Karbonski otisak je ${response.data.carbonFootprint}`);
          let typography = document.getElementById('target');
          typography.innerHTML = `Karbonski otisak je ${response.data.carbonFootprint}`
          let localUsers = JSON.parse(localStorage.getItem("users"));
          // let loginUser = JSON.parse(localStorage.getItem("loginUser"));
          localUsers.forEach(user => { // Menjanje registrovanog korisnika.
            if (user.username === loginUser.username) {
              if (!user.hasOwnProperty('carbonFootprint')) {
                user.carbonFootprint = Number(response.data.carbonFootprint);
              } else {
                user.carbonFootprint += Number(response.data.carbonFootprint);
              }
            }
          })
          if (!loginUser.hasOwnProperty('carbonFootprint')) {
            loginUser.carbonFootprint = Number(response.data.carbonFootprint);
          } else {
            loginUser.carbonFootprint += Number(response.data.carbonFootprint);
          }
          localStorage.setItem("loginUser", JSON.stringify(loginUser));
          localStorage.setItem("users", JSON.stringify(localUsers));
          let typography2 = document.getElementById('target2');
          typography2.innerHTML = `Vaš ukupan karbonski otisak je ${loginUser.carbonFootprint.toFixed(2)}`;
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed
        });
    }
  }

  useEffect(() => {
    let loginUser = JSON.parse(localStorage.getItem("loginUser"));
    let typography2 = document.getElementById('target2')
    if (!loginUser.carbonFootprint) { 
      console.log(loginUser.carbonFootprint) 
      typography2.innerHTML = 'Još uvek niste računali svoj karbonski otisak'
    } else {
      typography2.innerHTML = `Vaš ukupan karbonski otisak je ${loginUser.carbonFootprint.toFixed(2)}`
    }
  }, [])
  
  const mode = () => [
    { label: 'Automobil na dizel', value: 'dieselCar' },
    { label: 'Automobil na benzin', value: 'petrolCar' },
    { label: 'Ostali automobili', value: 'anyCar' },
    { label: 'Taksi', value: 'taxi' },
    { label: 'Let u ekonomskoj klasi', value: 'economyFlight' },
    { label: 'Let u biznis klasi', value: 'businessFlight' },
    { label: 'Let u prvoj klasi', value: 'firstclassFlight' },
    { label: 'Bilo koji let', value: 'anyFlight' },
    { label: 'Motocikl', value: 'motorbike' },
    { label: 'Autobus', value: 'bus' },
    { label: 'Tranzitna železnica', value: 'transitRail' }
  ]

  const country = () => [
    { label: 'Sjedinjene Američke Države', value: 'usa' },
    { label: 'Ujedinjeno Kraljevstvo', value: 'gbr' },
    { label: 'Ostale države', value: 'def' }
  ]

  const activityType = () => [
    { label: 'Kilometri', value: 'miles' },
    { label: 'Gorivo', value: 'fuel' }
  ]

  const fuelType = () => [
    { label: 'Motorni benzin', value: 'motorGasoline' },
    { label: 'Dizel', value: 'diesel' },
    { label: 'Avio-benzin', value: 'aviationGasoline' },
    { label: 'Mlazno gorivo', value: 'jetFuel' }
  ]

  return (
    <div>
      <NavBarActivities />

      <Paper className="background-image-profile"
        sx={{
          // height: 740,
          // Width: 910,
          backgroundImage: `linear-gradient(to bottom, rgba(172, 255, 47, 0.5), rgba(55, 86, 8, 0.5)), url(${Img})`,
          // backgroundImage: `url(${Img})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          minHeight: '100vh',
          maxWidth: `calc(98vw + 48px)`,
          flexDirection: 'column', 
          pt: 7
        }}
      >
        <Box className="content" sx={{ flex: 1, flexDirection: 'column' }}>
          <Container component='main' maxWidth='sm'>
            <CssBaseline />
            <Box
              sx={{
                // marginTop: 1,
                display: 'flex',
                // display: { xs: 'none', md: 'flex' },
                flexDirection: "column",
                alignItems: "center",
                bgcolor: "#ffff",
                px: 1.5,
                py: 2.5,
                boxShadow: 8
              }}
            >
              {/* DESKTOP VERZIJA - NASLOVA */}
              <Typography component="h1" variant="h5"
                sx={{
                  fontSize: 28,
                  mt: 2,
                  mb: 4,
                  display: { xs: 'none', md: 'flex' },
                  // textTransform: "uppercase" 
                }}
              >
                Izračunajte svoj karbonski otisak
              </Typography>

              {/* MOBILNA VERZIJA - NASLOVA */}
              <Typography /* component="h1" */ variant="h6"
                sx={{
                  fontSize: 21,
                  mt: 2,
                  mb: 3,
                  display: { xs: 'flex', md: 'none' },
                  // textTransform: "uppercase", 
                  alignText: "center"
                }} >
                Izračunajte svoj karbonski otisak
              </Typography>

              {/* // DESKTOP VERZIJA forme za Aktivnosti */}
              <Box sx={{ display: { xs: 'none', md: 'flex' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <DepartureBoardIcon />
                </Icon>
                <Autocomplete
                  disablePortal
                  id="activityType"
                  options={activityType()}
                  getOptionLabel={option => option.label}
                  sx={{ width: 350, m: 1 }}
                  renderInput={(params) => <TextField {...params} label="Razdaljina ili gorivo" />}
                  onChange={(e, razdaljinaIliGorivo) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, razdaljinaIliGorivo: Object.values(razdaljinaIliGorivo)[1] })
                    // Dohvatamo [1], jer je to value koja treba da bude prosljedjena backend-u.
                  }
                />
              </Box>

              <Box sx={{ display: { xs: 'none', md: 'flex' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <ShutterSpeedIcon />
                </Icon>
                {/* // InputProps={{ inputProps: { min: 0 } }} sprečava unos negativnih brojeva */}
                <TextField type="number" InputProps={{ inputProps: { min: 0 } }} inputMode="numeric" sx={{ width: 350, m: 1 }} id="outlined-basic" label="Razdaljina ili količina goriva" variant="outlined"
                  onChange={(e) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, brojMiljaIliGoriva: Number(e.target.value * 0.62137) })
                  }
                />
              </Box>

              <Box sx={{ display: { xs: 'none', md: 'flex' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <FlagIcon />
                </Icon>
                <Autocomplete
                  disablePortal
                  id="country"
                  options={country()}
                  sx={{ width: 350, m: 1 }}
                  renderInput={(params) => <TextField {...params} label="Zemlja" />}
                  onChange={(e, zemlja) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, zemlja: Object.values(zemlja)[1] })
                  }
                />
              </Box>

              <Box sx={{ display: { xs: 'none', md: 'flex' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <DirectionsCarIcon />
                </Icon>
                <Autocomplete
                  disablePortal
                  id="mode"
                  options={mode()}
                  sx={{ width: 350, m: 1 }}
                  renderInput={(params) => <TextField {...params} label="Vrsta prevoza" />}
                  onChange={(e, vrstaPrevoza) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, vrstaPrevoza: Object.values(vrstaPrevoza)[1] })
                  }
                />
              </Box>

              <Box sx={{ display: { xs: 'none', md: 'flex' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <LocalGasStationIcon />
                </Icon>
                <Autocomplete
                  disablePortal
                  id="fuelType"
                  options={fuelType()}
                  sx={{ width: 350, m: 1 }}
                  renderInput={(params) => <TextField {...params} label="Vrsta goriva" />}
                  onChange={(e, vrstaGoriva) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, vrstaGoriva: Object.values(vrstaGoriva)[1] })
                  }
                />
              </Box>

              {/* // VEĆI Izračunaj button */}
              {/* <Button
                onClick={request1}
                color="success"
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Izračunaj
              </Button> */}

              {/* // MANJI Izračunaj button */}
              <Button
                onClick={request1}
                color='success'
                type="submit"
                variant="contained"
                sx={{ mt: 4, mb: 2, width: 400, display: { xs: 'none', md: 'flex' } }}
              >
                Izračunaj
              </Button>

              <Button
                onClick={restart}
                color='success'
                type="submit"
                variant="contained"
                sx={{ mb: 2, width: 400, display: { xs: 'none', md: 'flex' } }}
              >
                Resetuj formu
              </Button>

              {/* //////////////////////////////////////////////////////////////////// */}
              {/* // MOBILNA VERZIJA forme za Aktivnosti */}
              <Box sx={{ display: { xs: 'flex', md: 'none' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <DepartureBoardIcon />
                </Icon>
                <Autocomplete
                  disablePortal
                  id="activityType"
                  options={activityType()}
                  getOptionLabel={option => option.label}
                  sx={{ width: 270, m: 1 }}
                  renderInput={(params) => <TextField {...params} label="Razdaljina ili gorivo" />}
                  onChange={(e, razdaljinaIliGorivo) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, razdaljinaIliGorivo: Object.values(razdaljinaIliGorivo)[1] })
                    // Dohvatamo [1], jer je to value koja treba da bude prosljedjena backend-u.
                  }
                />
              </Box>

              <Box sx={{ display: { xs: 'flex', md: 'none' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <ShutterSpeedIcon />
                </Icon>
                {/* // InputProps={{ inputProps: { min: 0 } }} sprečava unos negativnih brojeva */}
                <TextField type="number" InputProps={{ inputProps: { min: 0 } }} inputMode="numeric" sx={{ width: 270, m: 1 }} id="outlined-basic" label="Razdaljina ili količina goriva" variant="outlined"
                  onChange={(e) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, brojMiljaIliGoriva: Number(e.target.value * 0.62137) })
                  }
                />
              </Box>

              <Box sx={{ display: { xs: 'flex', md: 'none' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <FlagIcon />
                </Icon>
                <Autocomplete
                  disablePortal
                  id="country"
                  options={country()}
                  sx={{ width: 270, m: 1 }}
                  renderInput={(params) => <TextField {...params} label="Zemlja" />}
                  onChange={(e, zemlja) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, zemlja: Object.values(zemlja)[1] })
                  }
                />
              </Box>

              <Box sx={{ display: { xs: 'flex', md: 'none' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <DirectionsCarIcon />
                </Icon>
                <Autocomplete
                  disablePortal
                  id="mode"
                  options={mode()}
                  sx={{ width: 270, m: 1 }}
                  renderInput={(params) => <TextField {...params} label="Vrsta prevoza" />}
                  onChange={(e, vrstaPrevoza) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, vrstaPrevoza: Object.values(vrstaPrevoza)[1] })
                  }
                />
              </Box>

              <Box sx={{ display: { xs: 'flex', md: 'none' }, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Icon sx={{ color: 'green'/* , mr: 1  */ }}>
                  <LocalGasStationIcon />
                </Icon>
                <Autocomplete
                  disablePortal
                  id="fuelType"
                  options={fuelType()}
                  sx={{ width: 270, m: 1 }}
                  renderInput={(params) => <TextField {...params} label="Vrsta goriva" />}
                  onChange={(e, vrstaGoriva) =>
                    setPodaciZaSlanje({ ...podaciZaSlanje, vrstaGoriva: Object.values(vrstaGoriva)[1] })
                  }
                />
              </Box>

              {/* // VEĆI Izračunaj button */}
              {/* <Button
                onClick={request1}
                color="success"
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Izračunaj
              </Button> */}

              {/* // MANJI Izračunaj button */}
              <Button
                onClick={request1}
                color='success'
                type="submit"
                variant="contained"
                sx={{ mt: 3, mb: 2, width: 310, display: { xs: 'flex', md: 'none' } }}
              >
                Izračunaj
              </Button>

              <Button
                onClick={restart}
                color='success'
                type="submit"
                variant="contained"
                sx={{ mb: 2, width: 310, display: { xs: 'flex', md: 'none' } }}
              >
                Resetuj formu
              </Button>

              <Typography id='target'
                sx={{
                  // display: { xs: 'flex', md: 'none' },
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  my: 2,
                  backgroundColor: 'greenyellow',
                  width: 310,
                  height: 80,
                  fontSize: '1.25rem',
                }}
              >
              </Typography>
              <Typography id='target2'
                sx={{
                  // display: { xs: 'flex', md: 'none' },
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  textAlign: 'center',
                  my: 2,
                  backgroundColor: 'greenyellow',
                  width: 310,
                  height: 80,
                  fontSize: '1.25rem',
                }}
              >
              </Typography>

            </Box>
          </Container>
        </Box>
        <Box>
          {/* <Header onDrawerToggle={handleDrawerToggle} /> */}
          <Copyright className="copyright"
              sx={{
              display: 'flex',
              color: 'white',
              mt: 7.5,
              mb: 0.5,
              right: '50%',
              left: '50%',
              // position: 'fixed',
              alignContent: 'center',
              justifyContent: 'center',
              bottom: 0
              }}
          />
        </Box>
      </Paper>

    </div>
  )
}

export default Activities;
