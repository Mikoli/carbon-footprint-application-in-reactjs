import React, { useState } from "react";
import { useNavigate } from "react-router-dom"
import "./Register.css";
import NavBarAuth from '../../components/NavBarAuth';

import * as yup from "yup";
import { useFormik } from "formik";

import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
// import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import ForestIcon from '@mui/icons-material/Forest';
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
// import Img1 from '../Picture/Img1.jpg';
import Img2 from '../Picture/Img2.jpg';
import { Paper } from '@mui/material';

// Pasword Visibily Eye-icon ON/OFF

import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";


function Copyright(props) {
  return (
    <Typography className="copyright" variant="body2" color="#fff" align="center" {...props}>
      {'Copyright©'}
      <Link color="inherit" href="https://mui.com/">
         www.karbonskiotisak.com
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}

// initialValues predstavlja inicijalne vrednosti koje ce nam biti u register formi
const initialValues = {
  username: "",
  password: "",
  re_password: "",
  showPassword: false,
  show_re_password: false,
  name: "",
  address: "",
  phone: "",
  email: "",
};

const dodajVerifedPoljeZaSveRegistrovaneKorisnike = () => {

  let users = JSON.parse(localStorage.getItem("users")) || [];
  
  // Ne moramo koristiti localStorage.getItem('users'), mada taj nacin obezbedjuje vise bezbednosti
  for(let user of users){
    if(!user.hasOwnProperty('verified')){ //Sa ovim if-om je obezbedjeni da se registrovanom korisniku
      // upise verified = false polje, samo ako vec nema to polje
      user.verified = false;
    }
    // else{
    //   console.log("Vec ima polje")
    // }
  }
  localStorage.users = JSON.stringify(users);
}

export default function Register() {
  dodajVerifedPoljeZaSveRegistrovaneKorisnike();
  // Samo pozovemo funkciju da bi ona svim korisnicima upisala verified = false;

  const [values, setValues] = useState([])
  const navigate = useNavigate()

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleClickShowConfirmPassword = () => {
    setValues({
      ...values,
      showConfirmPassword: !values.showConfirmPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleMouseDownConfirmPassword = (event) => {
    event.preventDefault();
  };

  const [error, setError] = React.useState("")

  const onSignUp = async (values) => {
    try {

      // skupljamo podatke iz forme i pravimo novog korisnika
      // values je niz koji sadrzi podatke iz forme
      const newUser = {
        username: values.username,
        password: values.password,
        re_password: values.re_password,
        name: values.name,
        address: values.address,
        phone: values.phone,
        email: values.email,
        verified: false
      };


      let localUsers = JSON.parse(localStorage.getItem("users")) || [];
      // Promenljivoj localUsers se dodeljuju trenutne vrednosti iz Local Storage
      // (niz objekata ili prazan niz).


      for (let i = 0; i < localUsers.length; i++) {
        console.log(localUsers[i].username); // Gresio sam jer sam pisao localUsers.username[i]...
        if(localUsers[i].username === newUser.username && 
          localUsers[i].email === newUser.email) {
            alert('Korisničko ime i e-mail adresa već postoje.');
            return;
          }
      // Vazno je da gornji uslov bude pod prvo if, jer ako se stavi na 2. ili 3. mesto, taj alert se nikad nece ispisati.
          if(localUsers[i].username === newUser.username) {
            alert("Korisničko ime već postoji.");
            return;
          }
          if(localUsers[i].email === newUser.email) {
            alert('Ova e-mail adresa već postoji.');
            return;
          }
      // Ovde ne mora da se dodaje else, jer je slucaj uspesnog registrovanja ispod u kodu.
      }

      // DODATA FILTER METODA /////////////////////////////////////////////

      // let sameRegisterUser = localUsers.filter(user => //Filter metoda vraca niz.
      //   user.username === newUser.username || 
      //   user.email === newUser.email
      // )


      // if (sameRegisterUser !== [] ) {
      //   alert('Korisnisničko ime i/ili e-mail adresa već postoji.')
      //   return; //Bez return-a neće čak ni setError da pokaže.
      // }

      // FIND METODA ////////////////////////////////////////////////////

      // let userExistsWithSameUsername = localUsers.find(user => {
      //   return user.username === newUser.username;
      //   //Metoda find vraca ili element ili undefined ako ga ne nadje u nizu.
      // });
      // let userExistsWithSameEmail = localUsers.find(user => {
      //   return user.email === newUser.email;
      // });

      // if (userExistsWithSameUsername !== undefined) { // vec postoji korisnik sa istim username-om
      //   setError("Korisničko ime već postoji");
      //   return;
      // }
      // if (userExistsWithSameEmail !== undefined) { // vec postoji korisnik sa istim mejlom
      //   setError("Ova e-mail adresa već postoji");
      //   return;
      // }

      localUsers.push(newUser)
      // Ubacivanje novog korisnika u niz objekata nakon sto je zadovoljio uslove.

      localStorage.setItem("users", JSON.stringify(localUsers));
      // Ubacivanje registrovanih korisnika u Local Storage.

      alert("Uspešno ste se registrovali!")

      navigate("/login")
      navigate(0)

    } catch (error) {

      setError("Greška.");
      return;

    }
  }

  //     /////////////////////////////////////////////////////////////////
  //     // FOR..IN PETLJA kroz localStorage /////////////////////////////

  //     //localUsers name je kao "baza" svih registrovanih korisnika - to je zapravo niz registrovanih korisnika
  //     const localUsers = JSON.parse(localStorage.getItem("users")) || []
  //     // Promenljivoj localUsers se dodeljuju trenutne vrednosti iz Local Storage
  //     // (niz objekata ili prazan niz).

  //     for (let i in localUsers) { 

  //       console.log(localUsers[i].username);
  //       // newUser.username = localUsers[i].username;
  //       // newUser.email = localUsers[i].email;

  //       if (newUser.username === localUsers[i].username) { // vec postoji korisnik sa istim username-om
  //         setError("Korisničko ime već postoji.");
  //         return;
  //       }
  //       if (newUser.email === localUsers[i].email) { // vec postoji korisnik sa istim mejlom
  //         setError("Ova e-mail adresa već postoji.");
  //         return;
  //       }
  //     }

  //     localUsers.push(newUser);
  //     //Ubacivanje novog korisnika u niz objekata nakon sto je zadovoljio uslove.
  //     localStorage.setItem("users", JSON.stringify(localUsers));
  //     //Ubacivanje registrovanih korisnika u Local Storage.

  //     alert("Uspešno ste se registrovali!");

  //     navigate("/login");
  //     navigate(0);


  //   } catch (error) {

  //     setError("Greška.");
  //     return;

  //     // let err = Object.values(error.response.data)

  //     // if (err[0][0] === 'The password is too similar to the email address.') {
  //     //   setError('Lozinka ne sme biti slična imejlu.')
  //     // }
  //     // else if (err[0][0] === 'This password is too common.') {
  //     //   setError('Ova lozinka je uobičajena.')
  //     // }
  //     // else if (err[0][0] === 'A user with that username already exists.') {
  //     //   setError('Ovo korisničko ime već postoji.')
  //     // }
  //     // else {
  //     //   setError('Greška.')
  //     // }
  //     // return
  //   }
  // }

  const validationSchema = yup.object({
    username: yup.string()
      .required("Obavezno polje"),
    password: yup.string()
      .required("Obavezno polje")
      // .matches(
      //   /^(?=[a-zA-Z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,12}$)/,
      //   "Lozinka mora sadržati minimum 8 karaktera, maksimum 12, jedno veliko slovo, jedan broj i jedan specijalni karakter, i mora počinjati slovom"
      // ),
      // .matches(
      //   /^(?=[a-zA-Z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,12}$)/,
      //   "Neispravan format lozinke"
      // ),
      .min(8, "Minimum 8 karaktera")
      .max(12, "Maksimum 12 karaktera")
      .matches(
        /([A-Z])/,
        "Lozinka mora sadržati bar jedno veliko slovo"
      )
      .matches(
        /(\d)/,
        "Lozinka mora sadržati bar jedan broj"
      )
      .matches(
        /(\W)/,
        "Lozinka mora sadržati bar jedan specijalni karakter"
      )
      .matches(
        /^(?=[a-zA-Z])/,
        "Lozinka mora počinjati slovom"
      ),
    re_password: yup.string()
      .required("Obavezno polje")
      .oneOf([yup.ref("password"), null], "Lozinke se ne podudaraju"),
    name: yup.string()
      .required("Obavezno polje"),
    address: yup.string()
      .required("Obavezno polje"),
    phone: yup.string()
      .required("Obavezno polje"),
    email: yup.string()
      .email("Neispravan imejl")
      .required("Obavezno polje"),

  });

  const formik = useFormik({
    initialValues: initialValues,
    enableReinitialize: true,
    validationSchema: validationSchema,
    onSubmit: (values) => {
      onSignUp(values);
      formik.resetForm();
    }
  })

  return (
    <div>
      <NavBarAuth />

      <Paper className="background-image-home" 
        sx={{ 
          backgroundImage: `url(${Img2})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          minHeight: '100vh',
          maxWidth: `calc(98vw + 48px)`,
          flexDirection: 'column', 
          pt: 4
        }}
      >

        <Box className="content" sx={{ flex: 1, flexDirection: 'column'}}>
          <Container className="register-form" component="main" maxWidth="sm">
            <CssBaseline />
            <Box
              sx={{
                marginTop: 5,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                bgcolor: "#ffff",
                padding: 2.5,
                boxShadow: 8,
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: "success.main" }}>
                <ForestIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Registruj se
              </Typography>
              <Box component="form" onSubmit={formik.handleSubmit} sx={{ mt: 3 }}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      size="small"
                      color="success"
                      required
                      fullWidth
                      id="username"
                      label="Korisničko ime"
                      value={formik.values.username}
                      // onChange={(event)=>setUsername(event.target.value)}
                      onChange={formik.handleChange}
                      error={formik.touched.username && Boolean(formik.errors.username)}
                      helperText={formik.touched.username && formik.errors.username}
                      name="username"
                      autoComplete="username"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      required
                      size="small"
                      color="success"
                      fullWidth
                      name="password"
                      label="Lozinka"
                      type={values.showPassword ? "text" : "password"}
                      id="password"
                      autoComplete="new-password"
                      value={formik.values.password}
                      // onChange={(event)=>setPassword(event.target.value)}
                      onChange={formik.handleChange}
                      error={formik.touched.password && Boolean(formik.errors.password)}
                      helperText={formik.touched.password && formik.errors.password}

                      InputProps={{
                        endAdornment: <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                          >
                            {values.showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      required
                      size="small"
                      color="success"
                      fullWidth
                      name="re_password"
                      label="Potvrdi lozinku"
                      type={values.showConfirmPassword ? "text" : "password"}
                      value={formik.values.re_password}
                      onChange={formik.handleChange}
                      // onChange={(event)=>setRe_password(event.target.value)}
                      error={formik.touched.re_password && Boolean(formik.errors.re_password)}
                      helperText={formik.touched.re_password && formik.errors.re_password}
                      id="re_password"
                      autoComplete="new-password"

                      InputProps={{
                        endAdornment: <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowConfirmPassword}
                            onMouseDown={handleMouseDownConfirmPassword}
                            edge="end"
                          >
                            {values.showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      size="small"
                      color="success"
                      autoComplete="name"
                      name="name"
                      required
                      fullWidth
                      id="name"
                      label="Ime i prezime"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      // onChange={(event)=>setName(event.target.value)}
                      error={formik.touched.name && Boolean(formik.errors.name)}
                      helperText={formik.touched.name && formik.errors.name}
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      size="small"
                      color="success"
                      required
                      fullWidth
                      id="address"
                      label="Adresa (ulica i broj, grad)"
                      value={formik.values.address}
                      // onChange={(event)=>setAddress(event.target.value)}
                      onChange={formik.handleChange}
                      error={formik.touched.address && Boolean(formik.errors.address)}
                      helperText={formik.touched.address && formik.errors.address}
                      name="address"
                      autoComplete="address"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      size="small"
                      color="success"
                      required
                      fullWidth
                      id="phone"
                      label="Kontakt telefon"
                      value={formik.values.phone}
                      // onChange={(event)=>setPhone(event.target.value)}
                      onChange={formik.handleChange}
                      error={formik.touched.phone && Boolean(formik.errors.phone)}
                      helperText={formik.touched.phone && formik.errors.phone}
                      name="phone"
                      autoComplete="phone"
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <TextField
                      size="small"
                      color="success"
                      required
                      fullWidth
                      id="email"
                      label="Email adresa"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      error={formik.touched.email && Boolean(formik.errors.email)}
                      helperText={formik.touched.email && formik.errors.email}
                      name="email"
                      autoComplete="email"
                    />
                  </Grid>
                </Grid>
                <Button
                  color="success"
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 2.5, mb: 0.5 }}
                >
                  Registruj se
                </Button>
                {error !== "" && <p style={{ color: "success" }}>{error}</p>}
                <Grid container justifyContent="flex-end">
                  <Grid item>
                    <Link href="/login" variant="body2" color="success.main"
                    // onClick={navigateSignIn}
                    >
                      Već imate nalog? Ulogujte se.
                    </Link>
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Container>
        </Box>
        <Box>
          {/* <Header onDrawerToggle={handleDrawerToggle} /> */}
          <Copyright className="copyright"
              sx={{
              display: 'flex',
              color: 'white',
              mt: 6.5,
              mb: 0.5,
              right: '50%',
              left: '50%',
              position: 'fixed',
              alignContent: 'center',
              justifyContent: 'center',
              bottom: 0
              }}
          />
        </Box>
      </Paper>
    </div>
  );
}

