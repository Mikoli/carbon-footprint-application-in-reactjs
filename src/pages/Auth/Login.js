import React from 'react';
import { useNavigate } from 'react-router-dom';
import './Login.css';
import NavBarAuth from '../../components/NavBarAuth';

import * as yup from 'yup';
import { useFormik } from 'formik';

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
// import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import ForestIcon from '@mui/icons-material/Forest';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Img2 from '../Picture/Img2.jpg';
import { Paper } from '@mui/material';

// Password Visibily Eye-icon ON/OFF

import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

function Copyright(props) {
  return (
    <Typography className="copyright" variant="body2" color="#fff" align="center" {...props}>
      {'Copyright©'}
      <Link color="inherit" href="https://mui.com/">
         www.karbonskiotisak.com
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}

export default function Login() {

  const navigate = useNavigate()

  const [values, setValues] = React.useState({
    username: '',
    password: '',
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const [error, setError] = React.useState('')

  const onSignIn = async (values) => {
    try {
      const loginUser = {
        username: values.username,
        password: values.password,
      };

      let localUsers = JSON.parse(localStorage.getItem("users")) || []
      // Promenljivoj localUsers se dodeljuju trenutne vrednosti iz Local Storage
      // (niz objekata ili prazan niz).


      let retirevedUser = localUsers.filter(user => //Filter metoda vraca niz.
        user.username === loginUser.username &&
        user.password === loginUser.password
      )

      if (loginUser.username === 'Admin' && loginUser.password === 'Admin91$') {
        alert('Zdravo, Admin') //Validacija administratora
        navigate('/home-admin');
        navigate(0);
        return;
      }

      if (retirevedUser.length > 0) {
        // Ukoliko je ovaj uslov zadovoljen, to znaci da postoji korisnik u registarskom
        // nizu korisnika u Local Storage ciji se username i lozinka poklapaju sa onima
        // koji su uneti u Login formu.
        console.log(retirevedUser[0].verified)
        // Ispisace falce ili true, sto predstavlja proveru da li je admin odobrio korisnika.
        
        
          if (retirevedUser[0].verified === false) {
            alert('Vaš zahtev još nije odobren od strane administratora.');
            return;
          } else {
            alert('Uspešno ste se ulogovali')
            navigate('/profile');
            navigate(0);
          }
        

        localStorage.setItem("loginUser", JSON.stringify(retirevedUser[0]));
        // Upisivanje ulogovanog korisnika u poseban niz nakon sto je zadovoljio uslov za ulogovanog korisnika.
      }
      else {
        setError('Ne postoji korisnik sa tim kredencijalima')
      }


    } catch (error) {
      setError('Greška')
      return

    }
  }

  const validationSchema = yup.object({
    username: yup.string()
      .required('Obavezno polje'),
    password: yup.string()
      .required('Obavezno polje')
  });

  const formik = useFormik({
    initialValues: values,
    validationSchema: validationSchema,
    onSubmit: onSignIn,
  });

  return (
    <div>
      <NavBarAuth />

      <Paper className="background-image-home" 
        sx={{ 
          backgroundImage: `url(${Img2})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          minHeight: '100vh',
          maxWidth: `calc(98vw + 48px)`,
          flexDirection: 'column', 
          pt: 4
        }}
      >

        <Box className="content" sx={{ flex: 1, flexDirection: 'column' }}>
          <Container className="login-form" component="main" maxWidth="sm">
            <CssBaseline />
            <Box
              sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: "center",
                bgcolor: '#ffff',
                padding: 2.5,
                boxShadow: 8,
              }}
            >
              <Avatar className='avatar' sx={{ m: 1, bgcolor: 'success.main' }}>
                <ForestIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Uloguj se
              </Typography>
              <Box className="username" component="form" onSubmit={formik.handleSubmit} noValidate /* onSubmit={handleSubmit} */ sx={{ mt: 1 }}>
                <TextField
                  color="success"
                  margin="normal"
                  required
                  fullWidth
                  id="username"
                  label="Korisničko ime"
                  // onChange={(event)=>setUsername(event.target.value)}
                  value={formik.values.username}
                  onChange={formik.handleChange}
                  error={formik.touched.username && Boolean(formik.errors.username)}
                  helperText={formik.touched.username && formik.errors.username}
                  name="username"
                  autoComplete="username"
                />
                <TextField
                  color="success"
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Lozinka"
                  type={values.showPassword ? 'text' : 'password'}
                  // onChange={(event)=>setPassword(event.target.value)}
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  error={formik.touched.password && Boolean(formik.errors.password)}
                  helperText={formik.touched.password && formik.errors.password}
                  id="password"
                  autoComplete="current-password"

                  InputProps={{
                    endAdornment: <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {values.showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }}
                />
                <FormControlLabel
                  control={<Checkbox value="remember" color="success" align="right" />}
                  label="Zapamti me"
                />
                <Button
                  color="success"
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                >
                  Uloguj se
                </Button>
                {error !== '' && <p style={{ color: 'success.main' }}>{error}</p>}
                <Grid container>
                  <Grid item xs>
                    <Link href="#" variant="body2" color="success.main" position="start">
                      Zaboravljena lozinka?
                    </Link>
                  </Grid>
                  <Grid item>
                    <Link href="/register" className="signUp" variant="body2" color="success.main" position="end"
                    // onClick={navigateSignUp}
                    >
                      Nemate nalog? Registrujte se.
                    </Link>
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Container>
        </Box>
        <Box>
          {/* <Header onDrawerToggle={handleDrawerToggle} /> */}
          <Copyright className="copyright"
              sx={{
              display: 'flex',
              color: 'white',
              mt: 6.5,
              mb: 0.5,
              right: '50%',
              left: '50%',
              position: 'fixed',
              alignContent: 'center',
              justifyContent: 'center',
              bottom: 0
              }}
          />
        </Box>
      </Paper>
    </div>
  );
}
