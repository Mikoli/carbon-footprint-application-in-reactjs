import React from 'react';
import Img from '../Picture/Img.jpg';
import NavBarAdmin from '../../components/NavBarAdmin';
import { styled } from '@mui/material/styles';
import { green } from '@mui/material/colors';
import { Paper } from '@mui/material';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useState } from 'react';
import { useEffect } from 'react';
import { DataGrid } from '@mui/x-data-grid';

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(green[500]),
  backgroundColor: green[500],
  '&:hover': {
    backgroundColor: green[700],
  },
}));


function Copyright(props) {
  return (
    <Typography className="copyright" variant="body2" color="#fff" align="center" {...props}>
      {'Copyright©'}
      <Link color="inherit" href="https://mui.com/">
        www.karbonskiotisak.com
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}

const HomePageAdmin = () => {
  const [registerUsers, setRegisterUsers] = useState([]);
  const [registerUsersVerified, setRegisterUsersVerified] = useState([]);

  useEffect(() => {
  //     let a = localStorage.getItem('users')
  // console.log(typeof(a)) // Vratice null kada je prazan Local Storage
    if (localStorage.getItem('users') === null) {
      let users = [{
        'username': 'Mikoli',
        'password': 'Aaaaa11$',
        're_password': 'Aaaaa11$',
        'name': 'Miljana Milentijević',
        'address': 'Smederevo',
        'phone': '066111111',
        'email': 'mikoli@gmail.com',
        'verified': false
      }, {
        'username': 'Ivan',
        'password': 'Aaaaa11$',
        're_password': 'Aaaaa11$',
        'name': 'Ivan Đurđević',
        'address': 'Krupanj',
        'phone': '066222222',
        'email': 'ivan@gmail.com',
        'verified': false
      }, {
        'username': 'Dencha',
        'password': 'Aaaaa11$',
        're_password': 'Aaaaa11$',
        'name': 'Milan Denić',
        'address': 'Suva planina',
        'phone': '066333333',
        'email': 'milan@gmail.com',
        'verified': false
      }, {
        'username': 'Mikoli2',
        'password': 'Aaaaa11$',
        're_password': 'Aaaaa11$',
        'name': 'Miljana Milentijević',
        'address': 'Smederevo',
        'phone': '066111111',
        'email': 'mikoli@yahoo.com',
        'verified': false
      }, {
        'username': 'Ivan2',
        'password': 'Aaaaa11$',
        're_password': 'Aaaaa11$',
        'name': 'Ivan Đurđević',
        'address': 'Krupanj',
        'phone': '066222222',
        'email': 'ivan@yahoo.com',
        'verified': false
      }, {
        'username': 'Dencha2',
        'password': 'Aaaaa11$',
        're_password': 'Aaaaa11$',
        'name': 'Milan Denić',
        'address': 'Suva planina',
        'phone': '066333333',
        'email': 'milan@yahoo.com',
        'verified': false
      }]
      localStorage.setItem('users', JSON.stringify(users));
    }

    setRegisterUsers(JSON.parse(localStorage.users));
  }, [])

  useEffect(() => {
    const registerUsersVerified = registerUsers.filter(user => // Filter metoda vraca niz.
      user.verified === true
    );
    console.log(registerUsersVerified);
    setRegisterUsersVerified(registerUsersVerified);
  }, [registerUsers])

  const columns = [
    { field: 'name', headerName: 'Ime i prezime', width: 200 },
    { field: 'email', headerName: 'Mejl', width: 250 },
    {
      field: 'username',
      headerName: 'Korisničko ime',
      width: 160,
    },
    {
      field: "action",
      headerName: "Action",
      width: 180,
      description: 'This column has a value getter and is not sortable.',
      sortable: false,

      renderCell: (params) => {
        const deleteUser = e => {
          e.stopPropagation();
          let filteredRegisterUsers = registerUsers.filter(user => user.username !== params.row.username)
          setRegisterUsers(filteredRegisterUsers)
          localStorage.setItem('users', JSON.stringify(filteredRegisterUsers))
        }; 
        return <ColorButton variant="contained" sx={{ color: 'white', backgroundColor: 'green' }} onClick={deleteUser}>Obriši</ColorButton>;
      }
    }];


  return (
    <div>
      <NavBarAdmin />
      <Paper className="background-image-admin"
        sx={{
            // height: 740,
            // Width: 910,
            backgroundImage: `linear-gradient(to bottom, rgba(172, 255, 47, 0.45), rgba(55, 86, 8, 0.25)), url(${Img})`,
            // backgroundImage: `url(${Img})`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            minHeight: '100vh',
            maxWidth: `calc(98vw + 48px)`,
            flexDirection: 'column', 
            pt: 4
        }}
      >

        {/* DESKTOP VERZIJA - NASLOVA */}
        <Typography component="h1" variant="h5"
          sx={{
            fontSize: 26,
            mt: 2,
            mb: 5,
            display: { xs: 'none', md: 'flex' },
            // textTransform: "uppercase", 
            alignText: "center",
            alignContent: "center",
            justifyContent: "center"
          }}
        >
          Lista registrovanih korisnika
        </Typography>

        {/* MOBILNA VERZIJA - NASLOVA */}
        <Typography /* component="h1" */ variant="h6"
            sx={{
                fontSize: 21,
                mt: 2,
                mb: 3,
                display: { xs: 'flex', md: 'none' },
                // textTransform: "uppercase", 
                alignText: "center",
                alignContent: "center",
                justifyContent: "center"
            }}
        >
            Lista registrovanih korisnika
        </Typography>
        
        <Box sx={{ display: { xs: 'none', md: 'flex' }, height: 500, width: '60%', marginLeft: '20%', backgroundColor: 'ButtonFace' }}>
          <DataGrid
            rows={registerUsersVerified}
            columns={columns}
            getRowId={(row) => row.username}
            pageSize={10}
            rowsPerPageOptions={[10]}
            // checkboxSelection
          />
        </Box>
        <Box sx={{ display: { xs: 'flex', md: 'none' }, height: 500, width: '98%', marginLeft: '1%', backgroundColor: 'ButtonFace' }}>
          <DataGrid
            rows={registerUsersVerified}
            columns={columns}
            getRowId={(row) => row.username}
            pageSize={10}
            rowsPerPageOptions={[10]}
            // checkboxSelection
          />
        </Box>

        <Box>
          {/* <Header onDrawerToggle={handleDrawerToggle} /> */}
          <Copyright className="copyright"
            sx={{
              display: 'flex',
              color: 'white',
              mt: 6.5,
              mb: 0.5,
              right: '50%',
              left: '50%',
              position: 'fixed',
              alignContent: 'center',
              justifyContent: 'center',
              bottom: 0
            }}
          />
        </Box>

      </Paper>
    </div >
  )
}

export default HomePageAdmin;
