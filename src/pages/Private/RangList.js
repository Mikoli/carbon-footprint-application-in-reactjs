import * as React from 'react';
import Img from '../Picture/Img.jpg';
import './RangList.css';
import NavBarAdmin from '../../components/NavBarAdmin';

import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import Typography from "@mui/material/Typography";
import Link from '@mui/material/Link';

function Copyright(props) {
  return (
    <Typography className="copyright" variant="body2" color="#fff" align="center" {...props}>
      {'Copyright©'}
      <Link color="inherit" href="https://mui.com/">
         www.karbonskiotisak.com
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

let localUsers = JSON.parse(localStorage.getItem("users")) || []
let footprintUsers = localUsers.filter(user => //Filter metoda vraca niz.   
  user.hasOwnProperty('carbonFootprint'))
const rows = footprintUsers.sort((a, b) => (a.carbonFootprint < b.carbonFootprint ? -1 : 1));


export default function RangList() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  // Avoid a layout jump when reaching the last page with empty rows.   
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <div>
      <NavBarAdmin />

      <Paper className="background-image-admin"
        sx={{
          // height: 740,
          // Width: 910,
          backgroundImage: `linear-gradient(to bottom, rgba(172, 255, 47, 0.45), rgba(55, 86, 8, 0.25)), url(${Img})`,
          // backgroundImage: `url(${Img})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          minHeight: '100vh',
          maxWidth: `calc(98vw + 48px)`,
          flexDirection: 'column', 
          pt: 4
        }}
      >

        {/* DESKTOP VERZIJA - NASLOVA */}
        <Typography component="h1" variant="h5"
          sx={{
            fontSize: 26,
            mt: 2,
            mb: 5,
            display: { xs: 'none', md: 'flex' },
            // textTransform: "uppercase", 
            alignText: "center",
            alignContent: "center",
            justifyContent: "center"
          }}
        >
          Rang lista registrovanih korisnika
        </Typography>

        {/* MOBILNA VERZIJA - NASLOVA */}
        <Typography /* component="h1" */ variant="h6"
          sx={{
            fontSize: 21,
            mt: 2,
            mb: 3,
            display: { xs: 'flex', md: 'none' },
            // textTransform: "uppercase", 
            alignText: "center",
            alignContent: "center",
            justifyContent: "center"
          }}
        >
          Rang lista registrovanih korisnika
        </Typography>

        {/* //DESKTOP VERZIJA tabele */}
        {/* // sx = {{ display: { xs: 'none', md: 'flex' } }} */}
        {/* // Ova linija koda prikazuje verziju, samo na tabletima i desktop monitorima */}


        <TableContainer component={Paper} sx={{ display: { xs: 'none', md: 'flex' }, height: 500, width: '60%', marginLeft: '20%' }}>

          <Table sx={{ minWidth: 500, maxWidth: 6000, alignItems: "center", backgroundColor: '#ddffdd' }} aria-label="custom pagination table">
            <TableHead sx={{ backgroundColor: 'green' }}>
              <TableRow>
                <StyledTableCell /* align="center" */>R.br.</StyledTableCell>
                <StyledTableCell /* align="center" */>Ime i prezime</StyledTableCell>
                <StyledTableCell /* align="center" */>Korisničko ime</StyledTableCell>
                <StyledTableCell align="center" sx={{ textTransform: "uppercase" }}>Karbonski otisak</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody >
              {(rowsPerPage > 0
                ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                : rows
              ).map((row, index) => (
                <StyledTableRow key={row.name} >
                  <StyledTableCell component="th" scope="row" style={{ width: 20 }} >
                  {index + 1 + rowsPerPage * page}
                  </StyledTableCell>
                  <StyledTableCell style={{ width: 200 }} >
                    {row.name}
                  </StyledTableCell>
                  <StyledTableCell style={{ width: 180 }} >
                    {row.username}
                  </StyledTableCell>
                  <StyledTableCell style={{ width: 180 }} align="center">
                    {row.carbonFootprint.toFixed(2)}
                  </StyledTableCell>
                </StyledTableRow>
              ))}

              {emptyRows > 0 && (
                <StyledTableRow style={{ height: 53 * emptyRows }}>
                  <StyledTableCell colSpan={6} />
                </StyledTableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  // sx = {{display: { xs: 'none', md: 'flex' }}}
                  rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                  colSpan={3}
                  count={rows.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: {
                      'aria-label': 'rows per page',
                    },
                    native: true,
                  }}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>

        {/* //MOBILNA VERZIJA tabele */}
        {/* // sx = {{ display: { xs: 'flex', md: 'none' } }} */}
        {/* // Ova linija koda prikazuje verziju, samo na mobilnim telefonima */}

        <TableContainer component={Paper} sx={{ display: { xs: 'flex', md: 'none' }, height: 500, width: '98%', marginLeft: '1%'/* , m: 2 */ }}>

          <Table sx={{ backgroundColor: '#ddffdd' }} aria-label="custom pagination table">
            <TableHead sx={{ backgroundColor: 'green' }}>
              <TableRow>
                <StyledTableCell /* align="center" */>R.br.</StyledTableCell>
                <StyledTableCell /* align="center" */>Ime i prezime</StyledTableCell>
                <StyledTableCell /* align="center" */>Korisničko ime</StyledTableCell>
                <StyledTableCell align="center" sx={{ textTransform: "uppercase" }}>Karbonski otisak</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody >
              {(rowsPerPage > 0
                ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                : rows
              ).map((row, index) => (
                <StyledTableRow key={row.name} >
                  <StyledTableCell component="th" scope="row" style={{ width: 20 }} >
                  {index + 1 + rowsPerPage * page}
                  </StyledTableCell>
                  <StyledTableCell style={{ width: 200 }} >
                    {row.name}
                  </StyledTableCell>
                  <StyledTableCell style={{ width: 180 }} >
                    {row.username}
                  </StyledTableCell>
                  <StyledTableCell style={{ width: 180 }} align="center">
                    {row.carbonFootprint.toFixed(2)}
                  </StyledTableCell>
                </StyledTableRow>
              ))}

              {emptyRows > 0 && (
                <StyledTableRow style={{ height: 53 * emptyRows }}>
                  <StyledTableCell colSpan={6} />
                </StyledTableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  // sx = {{display: { xs: 'none', md: 'flex' }}}
                  rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                  colSpan={3}
                  count={rows.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: {
                      'aria-label': 'rows per page',
                    },
                    native: true,
                  }}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
        
        <Box>
          {/* <Header onDrawerToggle={handleDrawerToggle} /> */}
          <Copyright className="copyright"
              sx={{
              display: 'flex',
              color: 'white',
              mt: 6.5,
              mb: 0.5,
              right: '50%',
              left: '50%',
              position: 'fixed',
              alignContent: 'center',
              justifyContent: 'center',
              bottom: 0
              }}
          />
        </Box>

      </Paper>
    </div>
  );
}
