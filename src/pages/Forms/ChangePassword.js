import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import './ChangePassword.css';

import * as yup from 'yup';
import { useFormik } from 'formik';

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
// import Modal from '@mui/material/Modal';

import Spacer from '../../components/Spacer';

// Pasword Visibily Eye-icon ON/OFF

import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    border: 'none',
    boxShadow: 24,
    padding: 4
};

const initialValues = {
    current_password: '',
    new_password: '',
    re_new_password: ''
}

const ChangePassword = () => {
    const [values, setValues] = useState([]);
    const navigate = useNavigate();

    const handleClickShowCurrentPassword = () => {
        setValues({
            ...values,
            showCurrentPassword: !values.showCurrentPassword,
        });
    };

    const handleClickShowNewPassword = () => {
        setValues({
            ...values,
            showNewPassword: !values.showNewPassword,
        });
    };

    const handleClickShowConfirmNewPassword = () => {
        setValues({
            ...values,
            showConfirmNewPassword: !values.showConfirmNewPassword,
        });
    };

    const handleMouseDownCurrentPassword = (event) => {
        event.preventDefault();
    };

    const handleMouseDownNewPassword = (event) => {
        event.preventDefault();
    };

    const handleMouseDownConfirmNewPassword = (event) => {
        event.preventDefault();
    };

    const [error, setError] = React.useState('');

    const changePassword = async (values) => {
        try {
            // dohvatanje vrednosti iz forme za promenu sifre
            const current_password = values.current_password;
            const new_password = values.new_password;
            const re_new_password = values.re_new_password;

            // Dohvatanje vrednosti objekta ulogovanog korisnika
            let loginUser = JSON.parse(localStorage.getItem("loginUser"));
            // Promenljiva localUsers ce pokupiti vrednosti niza objekata registovanih korisnika iz Local Storage.
            let localUsers = JSON.parse(localStorage.getItem("users"))
            
            // IVANOV NAČIN //////////////////////////////////
            // let userRegisterWithSamePasswordIndex = localUsers.findIndex(user => {
            //     return user.password === loginUser.password && user.username === loginUser.username });
            // // Ova findIndex metoda vraca indeks (prvog) odgovarajuceg elementa, sto je u ovom slucaju registrovani korisnik koji je ulogovan.
            // // Ako ne nadje nista, vraca -1
            // // Ako bi se zakomentarisala ova find metoda nece posle da se izmeni sifre u Local Storage setItem metodom...
            // // Sledeca if provera je mozda i suvisna:
            // if (userRegisterWithSamePasswordIndex === -1) {
            //     alert('Uneta lozinka se ne poklapa sa lozinkom ulogovanog korisnika.')
            //     return
            // }
            // if (new_password === re_new_password && loginUser.password === current_password) {
            //     loginUser.password = new_password;
            // // Dodeljujemo nove vrednosti za password i repassword registrovanom i ulogovanom korisniku.
            // // Te nove vrednosti ce zatim biti setovane u Local Storage.
            //     loginUser.re_password = new_password;
            //     // userRegisterWithSamePassword.password = new_password;
            //     // userRegisterWithSamePassword.re_password = new_password;
            //     localUsers.splice(userRegisterWithSamePasswordIndex, 1);
            //     //Kod za brisanje jednog elementa iz niza, na osnovu indeksa elementa
            //     localUsers.push(loginUser);
            //     //Ubacujemo objekat korisnika sa novom sifrom u niz svih registrovanih korisnika
            //     localStorage.setItem("loginUser", JSON.stringify(loginUser));
            //     localStorage.setItem('users', JSON.stringify(localUsers))
            //     // Ovde je problem sto vise nije niz [0], pa ne moze korisnik ponovo da se uloguje kad se izloguje.
            //     alert('Uspešno ste promenili lozinku')
            //     navigate(`/profile`);
            //     navigate(0);

            // }


            // MILANOV NAČIN ///////////////////////////////
            if (loginUser.password !== current_password) {
                alert('Uneta lozinka se ne poklapa sa lozinkom ulogovanog korisnika!')
                return
            } else {
                console.log('Trenutni korisnik')
            }
            
            // TODO: proveri da li obe lozinke imaju najmanje 8 znakova umesto ovog if-a na liniji 56
            if (new_password === re_new_password && new_password.length >= 8 && new_password.length <= 12) {
                // izmenimo jedan element niza i ponovo postavimo ceo niz users u localStorage 
                localUsers.forEach(user => {
                    if (user.username === loginUser.username && user.password === loginUser.password) {
                        user.password = new_password
                        user.re_password = re_new_password
                        console.log("Trenutni")
                    } else (
                        console.log("Ne")
                    )
                })
                
                localStorage.setItem("users", JSON.stringify(localUsers))
                
                // izmenimo element loginUser i ponovo ga postavimo u localStorage
                loginUser.password = new_password
                loginUser.re_password = re_new_password
                localStorage.setItem("loginUser", JSON.stringify(loginUser))

                alert('Uspešno ste promenili lozinku')
                navigate(`/profile`);
                navigate(0);

            } else {
                alert("Lozinke se ne poklapaju ili ne ispunjavaju uslov dužine između 8 i 12 karaktera")
                navigate(`/profile`);
                navigate(0);
            }

            //////////////////////////////////////////////////////////////////

        } catch (error) {
            setError('Greška')
            return
        }
    }

    const validationSchema = yup.object({
        current_password: yup.string().required('Obavezno polje'),
        new_password: yup.string()
            .required('Obavezno polje')
            .min(8, 'Minimum 8 karaktera')
            .max(12, 'Maksimum 12 karaktera')
            .matches(
                /([A-Z])/,
                "Lozinka mora sadržati bar jedno veliko slovo"
              )
              .matches(
                /(\d)/,
                "Lozinka mora sadržati bar jedan broj"
              )
              .matches(
                /(\W)/,
                "Lozinka mora sadržati bar jedan specijalni karakter"
              )
              .matches(
                /^(?=[a-zA-Z])/,
                "Lozinka mora počinjati slovom"
              ),
        re_new_password: yup.string()
            .required('Obavezno polje')
            .oneOf([yup.ref('new_password'), null], ('Lozinke se ne podudaraju'))
    });

    const formik = useFormik({
        initialValues: initialValues,
        validationSchema: validationSchema,
        onSubmit: changePassword,
    });

    return (
        <div>
            <Box className="content" sx={{ flex: 1, flexDirection: 'column', border: 'none'}}>
                <Container className="change-password-form" sx={style} component="main" maxWidth="sm">
                    <CssBaseline />
                    <Typography /* m={3} */ component="h1" variant="h5" 
                        sx={{ 
                            mb: 5,
                            color: 'green',
                            textTransform: 'uppercase',
                            display: 'flex',
                            alignContent:'center', 
                            justifyContent: 'center'
                        }}
                    >
                        Promeni lozinku
                    </Typography>
                    <Box component="form" onSubmit={formik.handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField style={{ color: 'green' }}
                                    fullWidth
                                    id='current_password'
                                    label='Trenutna lozinka'
                                    name='current_password'
                                    variant='outlined'
                                    type={values.showCurrentPassword ? "text" : "password"}
                                    value={formik.values.current_password}
                                    onChange={formik.handleChange}
                                    error={formik.touched.current_password && Boolean(formik.errors.current_password)}
                                    helperText={formik.touched.current_password && formik.errors.current_password}
                                
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowCurrentPassword}
                                            onMouseDown={handleMouseDownCurrentPassword}
                                            edge="end"
                                        >
                                            {values.showCurrentPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                        </InputAdornment>
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='new_password'
                                    label='Nova lozinka'
                                    name='new_password'
                                    variant='outlined'
                                    type={values.showNewPassword ? "text" : "password"}
                                    value={formik.values.new_password}
                                    onChange={formik.handleChange}
                                    error={formik.touched.new_password && Boolean(formik.errors.new_password)}
                                    helperText={formik.touched.new_password && formik.errors.new_password}
                                
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowNewPassword}
                                            onMouseDown={handleMouseDownNewPassword}
                                            edge="end"
                                        >
                                            {values.showNewPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                        </InputAdornment>
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id='re_new_password'
                                    label='Ponovljena nova lozinka'
                                    name='re_new_password'
                                    variant='outlined'
                                    type={values.showConfirmNewPassword ? "text" : "password"}
                                    value={formik.values.re_new_password}
                                    onChange={formik.handleChange}
                                    error={formik.touched.re_new_password && Boolean(formik.errors.re_new_password)}
                                    helperText={formik.touched.re_new_password && formik.errors.re_new_password}
                                
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowConfirmNewPassword}
                                            onMouseDown={handleMouseDownConfirmNewPassword}
                                            edge="end"
                                        >
                                            {values.showConfirmNewPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                        </InputAdornment>
                                    }}
                                />
                            </Grid>

                            {error !== '' && <Grid item xs={12}><p style={{ color: 'red' }}>{error}</p></Grid>}
                            <Grid item xs={12}>
                                <Spacer height='1rem' />
                                <Button color='primary' variant='contained' fullWidth type='submit' style={{ backgroundColor: 'green' }}>
                                    Sačuvaj
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                </Container>
            </Box>
        </div>
    );
}

export default ChangePassword;