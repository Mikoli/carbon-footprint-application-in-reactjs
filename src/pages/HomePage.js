import React from 'react';
import './HomePage.css';
import NavBarActivities from '../components/NavBarActivities';

import Img from './Picture/Img.jpg';
import { Paper, Grid } from '@mui/material';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Cards from '../components/Cards';


function Copyright(props) {
  return (
    <Typography className="copyright" variant="body2" color="#fff" align="center" {...props}>
      {'Copyright©'}
      <Link color="inherit" href="https://mui.com/">
         www.karbonskiotisak.com
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}

const HomePage = () => {

  return (
    <div>
      <NavBarActivities />

      <Paper className="background-image-home" 
        sx={{ 
          backgroundImage: `url(${Img})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          minHeight: '100vh',
          maxWidth: `calc(98vw + 48px)`,
          flexDirection: 'column', 
          pt: 4
        }}
      >

        <Box className="content" sx={{ flex: 1, flexDirection: 'column' }}>

        <Typography className="heading-1" variant="h2" component="h2" my={5} /* mr={4} */ 
            sx={{
              display: 'flex',
              alignContent: 'center', 
              justifyContent: 'center',
              color: 'green',
              mx: '1rem',
              fontSize: {
                lg: 50,
                md: 50,
                sm: 60,
                xs: 36
              }
            }}
          >
            DEKARBONIZUJTE VAŠ OTISAK
          </Typography>

          <Typography variant="subtitle1" gutterBottom
            sx={{
              display: 'flex',
              alignContent: 'center', 
              justifyContent: 'center',
              textAlign: 'center',
              fontSize: {
                lg: 22,
                md: 22,
                sm: 22,
                xs: 18
              },
              color: 'green',
              mx: '1rem'
            }}
          >
            Ova aplikacija se bavi računanjem karbonskog otiska za prevozna sredstva koja su ponuđena.
          </Typography>

          <Typography variant="subtitle1" gutterBottom
            sx={{
              display: 'flex',
              alignContent: 'center', 
              justifyContent: 'center',
              alignText: 'center',
              textAlign: 'center',
              fontSize: {
                lg: 22,
                md: 22,
                sm: 22,
                xs: 18
              },
              color: 'green',
              mx: '1rem'
            }}
          >
            Pokušajte da ostavite što manje karbonskog traga na našoj planeti...
          </Typography>

          <Typography className="heading-2" variant="h2" component="h2" mt={12} mb={4}
            sx={{
              fontSize: {
                lg: 28,
                md: 28,
                sm: 30,
                xs: 24
              }
            }}
          >
            {/* KARBONSKI OTISCI U KILOGRAMIMA NA PREĐENIH 100 KM: */}
            Karbonski otisci u kilogramima na pređenih 100km:
          </Typography>
          


          <Grid container direction='row' sx={{ flex: 1, flexDirection: 'row' }}>
            <Grid item container>
              <Grid item xs={false} sm={1.5} /> {/* leva margina */}
              <Grid /* direction='column' */ item xs={12} sm={9} >
                <Cards />
              </Grid>
              <Grid item xs={false} sm={1.5} /> {/* desna margina */}
            </Grid>
          </Grid>

          {/* <Box
            component={Grid}
            item
            sm={4} 
            md={9}
            display={{ xs: "none", sm: "flex" }}
          >
            <Cards />
          </Box> */}

        </Box>
        <Box>
          {/* <Header onDrawerToggle={handleDrawerToggle} /> */}
          <Copyright className="copyright"
              sx={{
              display: 'flex',
              color: 'white',
              mt: 7.5,
              mb: 0.5,
              right: '50%',
              left: '50%',
              // position: 'fixed',
              alignContent: 'center',
              justifyContent: 'center',
              bottom: 0
              }}
          />
        </Box>
      </Paper>
    </div>
  )
}

export default HomePage;
