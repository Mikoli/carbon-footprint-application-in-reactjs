import React from 'react';
import Img2 from './Picture/Img2.jpg';
import './ProfilePage.css';

import { Paper } from '@mui/material';
import { Typography } from '@mui/material';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Skeleton from '@mui/material/Skeleton';
import NavBarProfile from '../components/NavBarProfile';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Link from '@mui/material/Link';


function Copyright(props) {
  return (
    <Typography className="copyright" variant="body2" color="#fff" align="center" {...props}>
      {'Copyright©'}
      <Link color="inherit" href="https://mui.com/">
         www.karbonskiotisak.com
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}

const ProfilePage = () => {

  let loginUser = JSON.parse(localStorage.getItem("loginUser"));
  // Dohvatanje ulogovanog korisnika iz Local Storage

  return (
    <div>
      <NavBarProfile />

      <Paper className="background-image-profile"
          sx={{
              height: 740,
              Width: 910,
              backgroundImage: `linear-gradient(to bottom, rgba(172, 255, 47, 0.45), rgba(55, 86, 8, 0.25)), url(${Img2})`,
              // backgroundImage: `url(${Img})`,
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              minHeight: '100vh',
              maxWidth: `calc(98vw + 48px)`,
              flexDirection: 'column', 
              pt: 4
          }}
      > 
        <Box className="content" sx={{ flex: 1, flexDirection: 'column'}}>
          <Container component='main' maxWidth='sm'>
            <CssBaseline />
            {/* <Typography className='page-title' variant="h6" component="h6" sx={{mt: 2}} >
              PROFILNA STRANA
            </Typography> */}

            <Typography component="h1" variant="h4"
              sx={{ 
                // fontSize: 28, 
                mt: 5,
                mb: 5,
                display: 'flex',
                // textTransform: "uppercase", 
                alignText: 'center',
                alignContent: 'center',
                justifyContent: 'center',
                color: 'green'
              }} 
            >
              Zdravo, {`${loginUser.username}!`}
            </Typography>

            {/* <Typography className="welcome-message" variant="h4" component="h4" sx={{mt: 5, mb:5}} >
              Zdravo, {`${loginUser.username}!`}
            </Typography> */}

            {loginUser ? (
              <Grid /* spacing={4} */ sx={{ flex: 1, flexDirection: 'column'}}>
                <Grid item>
                  <Grid item xs={1} />
                  <Grid item xs={10} md={12}>
                    <Box
                      sx={{
                        marginTop: 4,
                        bgcolor: '#ffff',
                        padding: 2.5,
                        boxShadow: 8,
                        borderRadius: 2
                      }}
                    >
                      <Typography component="h2" variant="h5"
                        sx={{ 
                          // fontSize: 22, 
                          mb: 2,
                          display: 'flex', 
                          alignItems: 'center', 
                          justifyContent: 'center',
                          textTransform: 'uppercase',
                        }}
                      >
                        Profil
                      </Typography>
                      <Divider variant="middle"  color="#9ffb54" />
                      <List variant="body2">
                        <ListItem>Korisničko ime: {`${loginUser.username}`}</ListItem>
                        <ListItem>Ime i prezime: {`${loginUser.name}`} </ListItem>
                        <ListItem>Adresa: {`${loginUser.address}`}</ListItem>
                        <ListItem>Telefon: {`${loginUser.phone}`} </ListItem>
                        <ListItem>E-mail: {`${loginUser.email}`}</ListItem>
                      </List>
                    </Box>
                  </Grid>
                  <Grid item xs={1} />
                </Grid>
              </Grid>) :
              <Stack spacing={1}>
                <Skeleton sx={{ bgcolor: 'grey.700' }} variant="circular" width={100} height={100} />
                <Skeleton sx={{ bgcolor: 'grey.700' }} variant="rectangular" width={410} height={150} />
              </Stack>
            }
          </Container>
        </Box>
        <Box>
          {/* <Header onDrawerToggle={handleDrawerToggle} /> */}
          <Copyright className="copyright"
              sx={{
              display: 'flex',
              color: 'white',
              mt: 6.5,
              mb: 0.5,
              right: '50%',
              left: '50%',
              position: 'fixed',
              alignContent: 'center',
              justifyContent: 'center',
              bottom: 0
              }}
          />
        </Box>
      </Paper>
    </div>
  )
}

export default ProfilePage;
