# CARBON FOOTPRINT Application  in ReactJS

During the two months of internship at Zühlke Group, I was working with a great team and amazing mentors, on Carbon Footprint application in ReactJS. We have successfully finalized the project, which is allowing logged in Users to calculate CO2 emissions based on a number of miles or amount of fuel, used for a different kind of vehicles, and also allowing them to track their total carbon footprint. Besides Registering and Logging In as a regular User, there's also an option for an Admin to login and access his own dashboard, where he can verify newly registered users, delete or see the Ranking List of all of them.

&nbsp;

![WP_MY_PORTFLOIO_CARBON_1](/uploads/dfac9f1115b9764fc5a320174829fa99/WP_MY_PORTFLOIO_CARBON_1.jpg)

![CARBON_group_1](/uploads/1b75af18dc8c048e30e98ecd4986af57/CARBON_group_1.jpg)

![WP_MY_PORTFLOIO_CARBON_4](/uploads/2093329e37191cdf4f8736dca3c11fc5/WP_MY_PORTFLOIO_CARBON_4.jpg)

![WP_MY_PORTFLOIO_CARBON_5](/uploads/d7546d20230fec30a8fc08eaac88ea34/WP_MY_PORTFLOIO_CARBON_5.jpg)

![WP_MY_PORTFLOIO_CARBON_3](/uploads/48f5568fa23c8dcf82f521d292df2991/WP_MY_PORTFLOIO_CARBON_3.jpg)

![CARBON_group_2](/uploads/8bf6936f07485b03362c78e284944c99/CARBON_group_2.jpg)

![WP_MY_PORTFLOIO_CARBON_2](/uploads/1aeac7d7fee7e997002dad515b3b040f/WP_MY_PORTFLOIO_CARBON_2.jpg)

![CARBON_group_3](/uploads/c33bcd99e93be4d691e87989e27fb160/CARBON_group_3.jpg)

&nbsp;

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
